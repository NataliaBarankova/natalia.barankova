import processing.serial.*;
Serial get;
float value;


void setup()
{
  size(600,600);
  // I know that the first port in the serial list on my mac
  // is Serial.list()[0].
  // On Windows machines, this generally opens COM1.
  // Open whatever port is the one you're using.
 get= new Serial(this, "/dev/cu.usbmodem14201", 9600);
 get.bufferUntil('\n');
}

void draw()
{
   background(0);

  stroke(#f1f1f1);
  if (value < 100) {
  strokeWeight(value*10);
  }
  else {
   strokeWeight(value/1000);
  }
  fill(#f1f1f1);
  rect(150,150,55,95,10,0,0,10);
  rect(205,245,95,55,0,0,10,0);
  rect(205,95,95,55,10,10,0,0);
  rect(300,150,55,95,0,0,10,0);
  rect(355,95,55,55,10,10,10,0);
  rect(150,300,55,55,10,0,0,10);
  rect(205,355,150,55,0,10,0,0);
  rect(205,465,150,55,0,0,10,10);
  rect(355,410,55,55,0,10,10,0);
  rect(150,410,55,55,10,0,0,10);
  }

void serialEvent (Serial get)
{
  value = float(get.readStringUntil('\n'));
}
