import paho.mqtt.client as mqtt
import time
import random
import serial

mqtt_broker = "netfabbcn.hopto.org"
mqtt_user = "students"
mqtt_pass = "fablabbcnisnice"
broker_port = 1883

PORT = "/dev/cu.usbmodem14201"
BAUDRATE = 115200
ser = serial.Serial(PORT, BAUDRATE)

def on_connect(client, userdata, flags, rc):
   print(f"Connected With Result Code: {rc}")

def on_message(client, userdata, message):
   print(f"Message Recieved: {message.payload.decode()}")
   # Do something here with the message

def on_log(client, obj, level, string):
    print (string)

def read_sensor():
	sensor_reading = random.randrange(0,1024)
	return sensor_reading

client = mqtt.Client(clean_session = True)
client.on_connect = on_connect
client.on_message = on_message
client.on_log = on_log
client.username_pw_set(username = mqtt_user, password = mqtt_pass)
client.connect(mqtt_broker, broker_port)


# Subscribe to your topic here
client.subscribe("output/naty", qos=1)
client.publish(topic="input/naty", payload="Hello from python", qos = 1, retain = False)


# Start looping (non-blocking)
client.loop_start()

while True:
	# Read data
    sensor_reading = read_sensor()
    client.publish(topic="input/naty", payload= str(sensor_reading), qos = 1, retain = False)
    time.sleep(5)
